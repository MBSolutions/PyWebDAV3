"""WebDAV library including a standalone server for python 3"""
__version__ = '0.9.14.4'
__author__ = 'Andrew Leech, Simon Pamies, Christian Scholz, Vince Spicer,\
    jasonlarose, Mathias Behrle'
__email__ = 'info@m9s.biz, andrew@alelec.net'
__license__ = 'LGPL v2'
